here I have included two functional test suite.

1. Deleting Employees Through Services by using Jmeter
this also helps in performance/load testing this application while login and then  deleting an employee

How it works?
clone the repo to local

You need to have JMeter instance running
Then simply load the provided interviewRequest.jmx file and hit run
additionally if you wish to delete ALL THE 3000 Employee records, just change the loop count -> under thread group to 3000. this will delete all the employees record.


2. Ruby+ SELENIUM +cucumber test to create an employee
for this i have provided both Feature file as well as cafe_townseed_setup_business_steps.rb Ruby file to run it independently

How it works?
You need to install Ruby 2.0 or higher and few ruby gems(have also attached the gem file required)
feature file can be loaded into any participating IDE that supports ruby such as ruby mine, atom, sublime text etc.
to run this ,
 open your console -> cd <path to the feature folder> cucumber --tags @Cafetownsend CHANNEL=firefox

alternately , you can just run the ruby file “cafe_townseed_setup_business_steps.rb”
independently to achieve the same
to run this ,
 open your console -> Ruby <path to cafe_townseed_setup_business_steps.rb file>

