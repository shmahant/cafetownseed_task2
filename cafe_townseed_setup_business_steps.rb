require 'rubygems'
require 'active_record'
require 'selenium-webdriver'
Selenium::WebDriver::Firefox::Binary.path = "/Applications/Firefox.app/Contents/MacOS/firefox"
$driver = Selenium::WebDriver.for :firefox, marionette: true
$driver.manage.window.maximize()


  $driver.navigate.to("http://cafetownsend-angular-rails.herokuapp.com/login")
  begin
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    select_item = wait.until {
      element = $driver.find_element(:xpath, "//input[@ng-model='user.name']")
      element if element.displayed?
    }
    select_item.send_keys "Luke"
  rescue Exception => e
    puts e.message
    $driver.quit
  end
  begin
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    select_item = wait.until {
      element = $driver.find_element(:xpath, "//input[@ng-model='user.password']")
      element if element.displayed?
    }
    select_item.send_keys "Skywalker"
  rescue Exception => e
    puts e.message
    $driver.quit
  end
  begin
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    select_item = wait.until {
      element = $driver.find_element(:xpath,  "//button[contains(.,'Login')]")
      element if element.displayed?
    }
    select_item.click
  rescue Exception => e
    puts e.message
    $driver.quit
  end
  sleep(2)


  begin
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    select_item = wait.until {
      element = $driver.find_element(:id,  'bAdd')
      element if element.displayed?
    }
    select_item.click
  rescue Exception => e
    puts e.message
    $driver.quit
  end
  sleep(4)
  $driver.find_element(:xpath, "//input[@ng-model='selectedEmployee.firstName']").send_keys "Hacked"
  $driver.find_element(:xpath, "//input[@ng-model='selectedEmployee.lastName']").send_keys "007"
  $driver.find_element(:xpath, "//input[@ng-model='selectedEmployee.startDate']").send_keys "2016-11-25"
  $driver.find_element(:xpath, "//input[@ng-model='selectedEmployee.email']").send_keys "hacker@hacking.com"
  sleep(1)
  $driver.find_element(:xpath, "//button[contains(.,'Add')]").click
  sleep(4)


  begin
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    select_item = wait.until {
      element = $driver.find_element(:class, "main-view-employees")
      element if element.displayed?
    }
    select_item.text.include? "Hacked"
  rescue Exception => e
    puts e.message
    $driver.quit
  end
  $driver.quit
